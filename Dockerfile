#Getting base image.  
FROM golang

# Go! Go! Gadget . . . errrr Go best practices!
MAINTAINER julie spradley (julie.spradley@gmail.com)

#Setting working directory
WORKDIR /app

# Copy the source from the current directory to the container's working directory
COPY . .

# Build the Go app
RUN go build -o main .

# Expose port 8080 to the universe
EXPOSE 8080

# Command to run the executable
CMD [go run go.main]